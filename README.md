# Installation

Run commands:

    composer require xsoft/fine-uploader
    php artisan fineUploader:install
    php artisan config:cache

# Usage

#####1. Include in your view
        
        @include('xsoft.fineUploader.combined')  
Remember to adjust `xsoft/fineUploader/combined.blade.php` file to your layout composition.
#####2. Add div where you want uploader to show.
```
<div id="my-fine-uploader"></div>
```
#####3. Call function on your div

     $('#my-fine-uploader').customFineUploader();
##Options
Most of options are the same as options specified in [FineUploader.js documentation](https://docs.fineuploader.com/branch/master/api/options.html).
     
     $('#my-fine-uploader').customFineUploader({
        'multiple' : true,
        'directory' : 'my_directory',
        'inputId' : 'my-input-id',
        'debug' : 1,
        'onCompleteCallback' : myJsFunction,
        'allowedExtensions' : ['jpg','png']
     });
     
####- multiple
Specify if input should accept multiple files. Default: false    
####- allowedExtensions
Array of allowed extensions. Default: [] - accept all     
####- directory
Specify directory in storage. Default directory is "fineUploader"
####- template
Specify template id.
####- inputId
Specify if input where file name will be injected after successful upload.
####- partSize
Specify chunk size that will be sent at a time. Default value can be managed in `config/fineUploader.php`
####- debug
Turns on or of fineUploader.js debug in console.
####- onCompleteCallback
####- onCancelCallback
####- onUploadCallback
####- onDeleteCallback
####- onDeleteCompleteCallback

## Configuration
You can configure fineUploader options in `config\fineUploader.php` file or by adding to your projects .env file:
```
FINEUPLOADER_CHUNKING_PARTSIZE=2000000
FINEUPLOADER_DEBUG=0
FINEUPLOADER_TMP_LIFESPAN=12
```

## Routes
    
    Route::prefix('fineUploader')->name('fineUploader.')->group(function () {
        Route::post('/upload', 'Xsoft\FineUploaderController@upload')->name('upload');
        Route::delete('/delete/{id?}', 'Xsoft\FineUploaderController@delete')->name('delete');
    });

In order to add authentication on fineUploader routes you can add those routes to your project web.php routes file and secure it in on your own.
