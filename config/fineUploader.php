<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Chunking
    |--------------------------------------------------------------------------
    |
    | FineUploader.js chunking options
    |   partSize - size of chunk in bytes
    |
    */
    'chunking' => [
        'partSize' => env('FINEUPLOADER_CHUNKING_PARTSIZE', 2000000)
    ],

    /*
    |--------------------------------------------------------------------------
    | Debugging
    |--------------------------------------------------------------------------
    |
    | Setting this option to true will enable FineUploader.js debug messages in console
    |
    */
    'debug' => env('FINEUPLOADER_DEBUG', 0),

    /*
    |--------------------------------------------------------------------------
    | Tmp Files Lifespan
    |--------------------------------------------------------------------------
    |
    |   Lifespan of tmp files defined in hours.
    |   Files older than set hours will be deleted by fineUploader::clearTmp command
    |
    */

    //lifetime of tmp files in hours
    'tmpFilesLifespan' => env('FINEUPLOADER_TMP_LIFESPAN', 12)
];
