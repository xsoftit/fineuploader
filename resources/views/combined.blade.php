@section('head')
    @parent
    @include('xsoft\fineUploader\head')
@endsection

@section('css')
    @parent
    @include('xsoft\fineUploader\css')
@endsection

@section('js')
    @parent
    @include('xsoft\fineUploader\script')
@endsection
