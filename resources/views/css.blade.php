<link href="/js/fu/fine-uploader-new.css" rel="stylesheet">
<style>
    #trigger-upload-fine-uploader {
        color: white;
        background-color: #00ABC7;
        font-size: 14px;
        padding: 7px 20px;
        background-image: none;
    }

    .fineUploader .qq-upload-button {
        margin-right: 15px;
    }

    .fineUploader .buttons {
        width: 36%;
    }

    .fineUploader .qq-uploader .qq-total-progress-bar-container {
        width: 60%;
    }
</style>
