<script src="/js/fu/fine-uploader.min.js"></script>
<script type="text/template" id="qq-template-fine-uploader">
    <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="{{__('app.fineUploader.dropFilesHere')}}">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                 class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector"></span>
        </div>
        <div class="buttons">
            <div class="qq-upload-button-selector qq-upload-button">
                <div>{{__('app.fineUploader.selectFiles')}}</div>
            </div>
            <button type="button"
                    class="btn btn-primary trigger-upload-fine-uploader">
                <i class="icon-upload icon-white"></i> {{__('app.fineUploader.upload')}}
            </button>
        </div>
        <span class="qq-drop-processing-selector qq-drop-processing">
                <span>{{__('app.fineUploader.processingDroppedFiles')}}</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
        <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
            <li>
                <div class="qq-progress-bar-container-selector">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                         class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                <span class="qq-upload-file-selector qq-upload-file"></span>
                <span class="qq-edit-filename-icon-selector qq-edit-filename-icon"
                      aria-label="Edit filename"></span>
                <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                <span class="qq-upload-size-selector qq-upload-size"></span>
                <button type="button"
                        class="qq-btn qq-upload-cancel-selector qq-upload-cancel">{{__('app.fineUploader.cancel')}}</button>
                <button type="button"
                        class="qq-btn qq-upload-retry-selector qq-upload-retry">{{__('app.fineUploader.retry')}}</button>
                <button type="button"
                        class="qq-btn qq-upload-delete-selector qq-upload-delete">{{__('app.fineUploader.delete')}}</button>
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
            </li>
        </ul>

        <dialog class="qq-alert-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">{{__('app.fineUploader.close')}}</button>
            </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">{{__('app.fineUploader.no')}}</button>
                <button type="button" class="qq-ok-button-selector">{{__('app.fineUploader.yes')}}</button>
            </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <input type="text">
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">{{__('app.fineUploader.cancel')}}</button>
                <button type="button" class="qq-ok-button-selector">{{__('app.fineUploader.ok')}}</button>
            </div>
        </dialog>
    </div>
</script>
