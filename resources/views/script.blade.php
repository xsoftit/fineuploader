<script data-title="betterFineUploader">
    var fineUploaderInstances = [];
    (function ($) {
        $.fn.customFineUploader = function (parameters) {
            let divId = $(this)[0].id;
            let directory = "fineUploader";
            let template = "qq-template-fine-uploader";
            let inputId = undefined;
            let partSize = {!! config('fineUploader.chunking.partSize') !!};
            let debug = {!! config('fineUploader.debug') !!};
            let multiple = false;
            let allowedExtensions = [];
            if (parameters !== undefined) {
                directory = (parameters.directory !== undefined) ? parameters.directory : "fineUploader";
                template = (parameters.templateId !== undefined) ? parameters.templateId : "qq-template-fine-uploader";
                inputId = parameters.inputId;
                multiple = (parameters.multiple !== undefined) ? parameters.multiple : false;
                partSize = (parameters.partSize !== undefined) ? parameters.partSize : {!! config('fineUploader.chunking.partSize') !!};
                debug = (parameters.debug !== undefined) ? parameters.debug : {!! config('fineUploader.debug') !!};
                allowedExtensions = (parameters.allowedExtensions !== undefined) ? parameters.allowedExtensions : [];
            }
            fineUploaderInstances[divId] = new qq.FineUploader({
                element: $(this)[0],
                template: template,
                messages: {
                    noFilesError: "{{__('app.fineUploader.messages.noFilesError')}}",
                    typeError: "{{__('app.fineUploader.messages.typeError')}}",
                    onLeave: "{{__('app.fineUploader.messages.onLeave')}}",
                },
                request: {
                    customHeaders: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                    endpoint: '{{route('fineUploader.upload')}}',
                    params: {
                        'storage_directory': directory
                    }
                },
                deleteFile: {
                    enabled: true,
                    forceConfirm: false,
                    customHeaders: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                    endpoint: '{{route('fineUploader.delete')}}',
                    params: {
                        'storage_directory': directory
                    }
                },
                thumbnails: {
                    placeholders: {
                        waitingPath: '/js/fu/placeholders/waiting-generic.png',
                        notAvailablePath: '/js/fu/placeholders/not_available-generic.png'
                    }
                },
                autoUpload: false,
                chunking: {
                    enabled: true,
                    mandatory: true,
                    partSize: partSize
                },
                debug: debug,
                multiple: multiple,
                validation: {
                    allowedExtensions: allowedExtensions,
                },
                callbacks: {
                    onComplete: function (id, name, responseJSON) {
                        if (inputId !== undefined) {
                            if (multiple) {
                                $('#' + inputId).val(responseJSON['file_name'] + " | " + $('#' + inputId).val());
                            } else {
                                $('#' + inputId).val(responseJSON['file_name']);
                            }
                        }
                        if (parameters !== undefined) {
                            (parameters.onCompleteCallback !== undefined) ? parameters.onCompleteCallback(id, name, responseJSON) : "";
                        }
                    },
                    onCancel: function (id, name) {
                        fineUploaderDeleteFile(fineUploaderInstances[divId].getUuid(id));
                        if (parameters !== undefined) {
                            (parameters.onCancelCallback !== undefined) ? parameters.onCancelCallback(id, name) : "";
                        }
                    },
                    onUpload: function (id, name) {
                        if (parameters !== undefined) {
                            (parameters.onUploadCallback !== undefined) ? parameters.onUploadCallback(id, name) : "";
                        }
                    },
                    onDelete: function (id) {
                        fineUploaderDeleteFile(fineUploaderInstances[divId].getUuid(id));
                        if (parameters !== undefined) {
                            (parameters.onDeleteCallback !== undefined) ? parameters.onDeleteCallback(id) : "";
                        }
                    },
                    onDeleteComplete: function (id, xhr, isError) {
                        if (parameters !== undefined) {
                            (parameters.onDeleteCompleteCallback !== undefined) ? parameters.onDeleteCompleteCallback(id, xhr, isError) : "";
                        }
                    }
                }
            });

            qq($("#" + divId + " .trigger-upload-fine-uploader")[0]).attach("click", function () {
                fineUploaderInstances[divId].uploadStoredFiles();
            });

            function fineUploaderDeleteFile(qqUuid) {
                let url = '{{route('fineUploader.delete',['id'=>'ID_PLACEHOLDER'])}}';
                url = url.replace('ID_PLACEHOLDER', qqUuid);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    url: url,
                    data: {
                        'storage_directory': directory
                    },
                    dataType: 'json',
                    method: 'DELETE',
                    success: function () {
                    }
                });
            }
        };
    })(jQuery);
</script>
