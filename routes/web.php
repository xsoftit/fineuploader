<?php

Route::prefix('fineUploader')->name('fineUploader.')->group(function () {
    Route::post('/upload', 'App\Http\Controllers\Xsoft\FineUploaderController@upload')->name('upload');
    Route::delete('/delete/{id?}', 'App\Http\Controllers\Xsoft\FineUploaderController@delete')->name('delete');
});
