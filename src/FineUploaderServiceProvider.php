<?php

namespace Xsoft\FineUploader;

use Illuminate\Support\ServiceProvider;

class FineUploaderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->publishes([
            __DIR__ . '/controllers/FineUploaderController.php' => app_path('/Http/Controllers/Xsoft/FineUploaderController.php'),
        ], 'xsoft-fineUploader-controller');
        $this->publishes([
            __DIR__ . '/commands/FineUploaderClearTmp.php' => app_path('/Console/Commands/FineUploaderClearTmp.php'),
        ], 'xsoft-fineUploader-command');
        $this->publishes([
            __DIR__ . '/../resources/js/fu' => public_path('js/fu')
        ], 'xsoft-fineUploader-fineUploaderJs');

        $this->publishes([
            __DIR__ . '/../resources/views' => resource_path('views/xsoft/fineUploader')
        ], 'xsoft-fineUploader-views');
        $this->publishes([
            __DIR__ . '/../config/' => config_path(),
        ], 'xsoft-fineUploader-config');
        if ($this->app->runningInConsole()) {
            $this->commands([
                FineUploaderInstallCommand::class,
            ]);
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__ . '/../routes/web.php';
    }
}
