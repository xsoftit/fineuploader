<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class FineUploaderClearTmp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fineUploader:clearTmp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $files = Storage::files('fineUploader/data');
            foreach ($files as $file) {
                $storage_file = unserialize(Storage::get($file));
                $last_modified = Storage::lastModified($file);
                $diff_in_hours = Carbon::parse(date('d.m.Y H:i:s', $last_modified))->diffInHours(now());
                if ($diff_in_hours > config('fineUploader.tmpFilesLifespan') || $storage_file['done'] == "true") {
                    $name = basename($storage_file['tmpname'], '.chunk');
                    if (Storage::exists('fineUploader/' . $name . ".chunk")) {
                        Storage::delete('fineUploader/' . $name . ".chunk");
                    }
                    if (Storage::exists('fineUploader/data/' . $name . ".dat")) {
                        Storage::delete('fineUploader/data/' . $name . ".dat");
                    }
                }
            }
        } catch (\Exception $exception) {
        }
    }
}
