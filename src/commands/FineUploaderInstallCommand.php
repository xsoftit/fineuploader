<?php

namespace Xsoft\FineUploader;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class FineUploaderInstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fineUploader:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->install();
        echo 'DONE';
    }

    protected function install()
    {
        echo "Published:" . PHP_EOL;
        Artisan::call('vendor:publish', ['-q' => '', "--tag" => "xsoft-fineUploader-config", '--force' => 'true']);
        echo "--config" . PHP_EOL;
        Artisan::call('vendor:publish', ['-q' => '', "--tag" => "xsoft-fineUploader-fineUploaderJs", '--force' => 'true']);
        echo "--fineUploader.js" . PHP_EOL;
        Artisan::call('vendor:publish', ['-q' => '', "--tag" => "xsoft-fineUploader-views", '--force' => 'true']);
        echo "--views" . PHP_EOL;
        Artisan::call('vendor:publish', ['-q' => '', "--tag" => "xsoft-fineUploader-controller", '--force' => 'true']);
        echo "--controller" . PHP_EOL;
        Artisan::call('vendor:publish', ['-q' => '', "--tag" => "xsoft-fineUploader-command", '--force' => 'true']);
        echo "--command" . PHP_EOL;
    }
}
