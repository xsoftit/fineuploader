<?php

namespace App\Http\Controllers\Xsoft;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FineUploaderController extends Controller
{
    public function upload(Request $request)
    {
        logger("Fine_Uploader | Uploading " . $request->get('qqfilename') . " | " . json_encode($request->all()));
        if ($request->get("ggfile")) {
            if ($request->get('ggfile')['error'] != 0) {
                header('HTTP/1.1 500 Uploaded file error');
                return response()->json(['success' => false]);
            }
        }
        $directory = $request->get('storage_directory');
        $info = $this->getFileInfo($directory, $_POST);

        if (!Storage::exists($directory)) {
            mkdir(Storage::path($directory), 0775);
        }
        if (!Storage::exists($directory . '/data')) {
            mkdir(Storage::path($directory . '/data'), 0775);
        }
        file_put_contents($info['tmpname'], file_get_contents($_FILES['qqfile']['tmp_name']), FILE_APPEND);
        $info['uploadedChunks'] += 1;
        $info['totalChunks'] = $_POST['qqtotalparts'];
        $info['uploadedSize'] += $_FILES['qqfile']['size'];
        $info['done'] = 'false';

        $this->putFileInfo($directory, $info);

        header('Content-Type: application/json');
        header('Chunk-Saved: ' . $info['uploadedChunks'] . '/' . $info['totalChunks']);

        if ($info['totalSize'] == $info['uploadedSize'] && $info['totalChunks'] == $_POST['qqtotalparts']) {
            $new_name = $info['fileOriginalName'];
            if (Storage::exists($directory . '/' . $info['fileOriginalName'])) {
                $new_name = date("Y_m_d_H_i_s_") . $info['fileOriginalName'];
            }
            rename($info['tmpname'], storage_path('app/' . $directory . '/') . $new_name);
            $info['file_name'] = $new_name;
            $info['done'] = 'true';
            $this->putFileInfo($directory, $info);
//            if (Storage::exists($directory . '/data/' . $_POST['qquuid'] . '.dat')) {
//                Storage::delete($directory . '/data/' . $_POST['qquuid'] . '.dat');
//            }
            return response()->json(['success' => true, 'file_name' => $new_name, 'file_size' => $info['totalSize']]);
        }
        return response()->json(['success' => true]);
    }

    private function getFileInfo($directory, $post)
    {
        $filename = Storage::path($directory . '/data/' . $post['qquuid'] . '.dat');
        $tempname = Storage::path($directory . '/' . $post['qquuid'] . '.chunk');
        if (file_exists($filename)) {
            return unserialize(file_get_contents($filename));
        } else {
            return array(
                'type' => $_FILES['qqfile']['type'],
                'fileOriginalName' => $post['qqfilename'],
                'tmpname' => $tempname,
                'totalSize' => $post['qqtotalfilesize'],
                'uploadedChunks' => 0,
                'totalChunks' => $post['qqtotalparts'],
                'uploadedSize' => 0
            );
        }
    }

    private function putFileInfo($directory, $info)
    {
        $filename = storage_path('app/' . $directory . '/data/' . $_POST['qquuid'] . '.dat');
        file_put_contents($filename, serialize($info));
    }

    public function delete(Request $request, $id)
    {
        $directory = $request->get('storage_directory');
        $dat_deleted = false;
        $chunk_deleted = false;
        $file_deleted = false;
        if (Storage::exists($directory . '/data/' . $id . '.dat')) {
            logger("Fine_Uploader | Deleting files");
            sleep(1);
            $path = Storage::path($directory . '/data/' . $id . '.dat');
            $info = unserialize(file_get_contents($path));
            if ($info['done'] == 'true') {
                if (Storage::exists($directory . '/' . $info['file_name'])) {
                    Storage::delete($directory . '/' . $info['file_name']);
                    $file_deleted = true;
                    logger("Fine_Uploader | Deleted: " . $directory . "/" . $info["file_name"]);
                }
            } else {
                if (Storage::exists($directory . '/' . $id . '.chunk')) {
                    Storage::delete($directory . '/' . $id . '.chunk');
                    $chunk_deleted = true;
                    logger("Fine_Uploader | Deleted: " . $directory . "/" . $id . ".chunk");
                }
            }
            Storage::delete($directory . '/data/' . $id . '.dat');
            logger("Fine_Uploader | Deleted: " . $directory . "/data/" . $id . ".dat");
            $dat_deleted = true;
        }
        return response()->json(['dat_deleted' => $dat_deleted, 'chunk_deleted' => $chunk_deleted, 'file_deleted' => $file_deleted, 'id' => $id]);
    }
}
